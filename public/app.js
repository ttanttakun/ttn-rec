var ttnRecorder = angular.module('ttn-rec', ['angularMoment', 'pathgather.popeye']);

ttnRecorder.controller('main', [
  '$scope',
  'Recorder',
  '$window',
  'Popeye',
  '$http',
  function($scope, Recorder, $window, Popeye, $http){
    $scope.recorders = {};

    var cache = $window.localStorage.getItem('recorders');
    if(cache) {
      angular.forEach(JSON.parse(cache), function(val, key){
        var newRecorder  = new Recorder(val);
        $scope.recorders[newRecorder.id] = newRecorder;
      });
    }

    $scope.start = function(){
      var newRecorder = new Recorder();
      newRecorder.start().then(function(){
        $scope.recorders[newRecorder.id] = newRecorder;
        $window.localStorage.setItem('recorders', JSON.stringify($scope.recorders));
      });
    };

    $scope.stop = function(recorder){
      $scope.recorders[recorder.id].stop().then(function(){
        $window.localStorage.setItem('recorders', JSON.stringify($scope.recorders));
      });
    };

    $scope.delete = function(recorder){
      if(confirm('Seguru al zaude?')) __delete();

      function __delete(){
        if(recorder.status === 'recording') {
          recorder.stop().then(function(){
            delete $scope.recorders[recorder.id];
            $window.localStorage.setItem('recorders', JSON.stringify($scope.recorders));
          });
        } else {
          delete $scope.recorders[recorder.id];
          $window.localStorage.setItem('recorders', JSON.stringify($scope.recorders));
        }

        $http.delete('/rec/' + recorder.id).then(function(res){

        });
      }
    };

    $scope.openModal = function(file) {
     var modal = Popeye.openModal({
       templateUrl: "modal.html",
       controller: "modalCtrl as ctrl",
       resolve: {
         irratsaioak: function($http) {
           return $http.get('/irratsaioak');
         },
         file: function($q) {
           var deferred = $q.defer();
            deferred.resolve(file);
           return deferred.promise;
         }
       }
     });

     // Show a spinner while modal is resolving dependencies
     $scope.showLoading = true;
     modal.resolved.then(function() {
       $scope.showLoading = false;
     });

     modal.closed.then(function() { });
   };
  }
]);

ttnRecorder.filter('status', [function(){
  return function(recorders, status) {
    var out = [];
    angular.forEach(recorders, function(val, key){
      if(val.status === status) {
        out.push(val);
      }
    });

    return out;
  };
}]);

ttnRecorder.filter('orderByDate', [function(){
  return function(recorders) {
    return recorders.sort(function(a, b) {
      return a.stopDate < b.stopDate
    });
  };
}]);

ttnRecorder.factory('Recorder', [
  '$http',
  '$q',
  '$window',
  '$interval',
  function(
    $http,
    $q,
    $window,
    $interval
  ){

  function Recorder(recorder){
    if(recorder){
      this.id = recorder.id;
      this.status = recorder.status;
      this.startDate = recorder.startDate;
      this.stopDate = recorder.stopDate;
      if(!this.stopDate) this.__timer();
    } else {
      this.id = undefined;
      this.status = 'new';
      this.startDate = undefined;
      this.stopDate = undefined;
    }
  }

  Recorder.prototype.__timer = function(stop){
    var self = this;
    if(stop) {
      return $interval.cancel(self.interval);
    }
    var delta = moment().subtract(moment(self.startDate));
    self.timer = delta.format('HH:mm:ss');
    self.interval = $interval(function(){
      var delta = moment().subtract(moment(self.startDate));
      self.timer = delta.format('HH:mm:ss');
    }, 1000);
  };

  Recorder.prototype.duration = function(){
    var self = this;
    var delta = moment(self.stopDate).subtract(moment(self.startDate));

    return delta.format('HH:mm:ss');
  };

  Recorder.prototype.start = function(){
    var self = this;
    var deferred = $q.defer();
    $http.post('/rec').success(function(res){
      self.id = res.id;
      self.status = 'recording';
      self.startDate = new Date();
      self.__timer();

      return deferred.resolve(res);
    });

    return deferred.promise;
  };

  Recorder.prototype.stop = function(){
    var self = this;
    var deferred = $q.defer();
    $http.post('/rec/'+ self.id + '/stop').success(function(res){
      self.status = 'stopped';
      self.stopDate = new Date();
      self.__timer(true);

      return deferred.resolve(res);
    });

    return deferred.promise;
  };

  return Recorder;
}]);

ttnRecorder.controller('modalCtrl', [
  'irratsaioak',
  '$scope',
  'file',
  '$http',
  'Popeye',
  function(irratsaioak, $scope, file, $http, Popeye){
    $scope.irratsaioak = irratsaioak.data;
    $scope.post = {
      file: file.id
    };
    $scope.upload = function(){
      if(!$scope.post.irratsaioa){
        return;
      }

      $http.post('/post', $scope.post).then(function(res){
        alert("Grabazioa igo da. Eskerrik asko!");
      });
      Popeye.closeCurrentModal($scope.post);
    };
  }
]);
