#!/bin/bash
if [ ${REALTIME:-false} == true ]; then
  REALTIME_OPT=-R
else
  REALTIME_OPT=-r
fi

echo "jackd $REALTIME_OPT -d net -a ${JACK_SERVER:-'127.0.0.1'} -n ${CLIENT_NAME:-'rasfm-rec'} 1> /dev/null 2> /dev/null &"
jackd $REALTIME_OPT -d net -a ${JACK_SERVER:-'127.0.0.1'} -n ${CLIENT_NAME:-'rasfm-rec'} 1> /dev/null 2> /dev/null &
sleep ${JACK_TIMEOUT:-5}
jack-plumbing -q -o -u 100000 ${PLUMBING_FILE:-\/etc\/rasfm-rec\/jack.plumbing} 2> /dev/null &
exec npm start
