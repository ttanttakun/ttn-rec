// expire?

var fse = require('fs-extra');
var express = require('express');
var cors = require('cors');
var spawn = require('child_process').spawn;
var moment = require('moment');
var request = require('request');
var bodyParser = require('body-parser')

var app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors());
app.use(express.static('public'));

if( ! process.env.IRRATSAIOAK_API || ! process.env.FLOWR_API || ! process.env.REC_SERVER ) {
  console.error("ERROR", "Missing env vars");
  process.exit(1);
}

var IRRATSAIOAK_API = process.env.IRRATSAIOAK_API;
var FLOWR_API = process.env.FLOWR_API;
var REC_SERVER = process.env.REC_SERVER;
var RECORDINGS_DIR = process.env.RECORDINGS_DIR || '/tmp/ttn/recordings/';

var recorders = {};

fse.ensureDir(RECORDINGS_DIR, function(err) {
  if(err) {
    console.error(err);
    process.exit(1);
  }
});

var server1 = app.listen(process.env.PORT || 3000, function () {
  var host = server1.address().address;
  var port = server1.address().port;
  console.log('Example app listening at http://%s:%s', host, port);
});

app.post('/rec', function (req, res) {
    var id = startRecording();
    res.json({id: id});
});

app.post('/rec/:id/stop', function(req, res){
  stopRecording(req.params.id);
  res.json({id: req.params.id});
});

app.get('/irratsaioak', function(req, res){
  request({
    method: 'GET',
    uri: IRRATSAIOAK_API,
    json: true
  }, function(err, response, body) {
    return res.json(body);
  });
});

app.post('/post', function (req, res) {
  var body = req.body;
  body.file = REC_SERVER + 'rec/' + body.file;
  request({
    method: 'POST',
    uri: FLOWR_API,
    body: body,
    json: true
  }, function(err, response, body) {
    return res.sendStatus(200);
  });
});

app.get('/rec/:id', function(req, res){
  res.download(RECORDINGS_DIR + req.params.id + '.mp3');
});

app.delete('/rec/:id', function(req, res){
 fse.remove(RECORDINGS_DIR + req.params.id + '.mp3', function(err){
   if(err){
      console.error("error deletng file");
   }
 });
  return res.sendStatus(200);
});

function startRecording(){
  var dateString = moment().format('HHmmssSSS');
  var recorder = spawn('ffmpeg',
  [
    '-f',
    'jack',
    '-i',
    'rasfm-rec-'+dateString,
    '-ab','320k',
    RECORDINGS_DIR + dateString + '.mp3', '-report'
  ], {
   cwd: RECORDINGS_DIR,
   stdio:  ['ignore', 'ignore', 'ignore']
 });

  if( process.env.JACK_CONNECT ){
    // wait a bit (recorder initialization) before connecting
    // it's better to use jack-plumbing
    setTimeout(function(){
      spawn('jack_connect', ['system:capture_2', 'rasfm-rec-' + dateString + ':input_2']);
      spawn('jack_connect', ['system:capture_1', 'rasfm-rec-' + dateString + ':input_1']);
    }, process.env.JACK_CONNECT_DELAY || 500);
  }

  recorders[dateString] = recorder;

  return dateString;
}

function stopRecording(id){
  if(recorders[id]){
    recorders[id].kill();
    delete recorders[id];
  }
  return id;
}
