FROM registry.gitlab.com/ttn-devops/docker-jackd

RUN apt-get update && apt-get install -y curl && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs ffmpeg

WORKDIR /opt/app

COPY package.json package-lock.json ./
RUN npm install
COPY index.js .
COPY public public
COPY etc/rasfm-rec/jack.plumbing /etc/rasfm-rec/
COPY start.sh /usr/local/bin/start.sh

CMD ["start.sh"]
